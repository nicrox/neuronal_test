console.log("**Test NeuralNetwork**");

var moduloN = require('brain');
var memoria = require('os');

console.log((memoria.freemem()/1024)/1024); /*memoria en uso despues del proceso*/

var net = new moduloN.NeuralNetwork(  {hiddenLayers: [5]});

/*Muestras necesarias para el aprendizaje*/
var data=[

/*Sin jugada*/
		{input: { c1: 1, c2: 0, c3: 0 ,c4: 0, c5: 0}, output: { nada: 1 }},
		{input: { c1: 0, c2: 1, c3: 0 ,c4: 0, c5: 0}, output: { nada: 0 }},
		{input: { c1: 0, c2: 0, c3: 1 ,c4: 0, c5: 0}, output: { nada: 1 }},
		{input: { c1: 0, c2: 0, c3: 0 ,c4: 1, c5: 0}, output: { nada: 0 }},
		{input: { c1: 0, c2: 0, c3: 0 ,c4: 0, c5: 1}, output: { nada: 1 }},
/*pareja*/
		{input: { c1: 1, c2: 1, c3: 0 ,c4: 0, c5: 0}, output: { pareja: 1 }},
		{input: { c1: 1, c2: 0, c3: 1 ,c4: 0, c5: 0}, output: { pareja: 1 }},
		{input: { c1: 1, c2: 0, c3: 0 ,c4: 1, c5: 0}, output: { pareja: 1 }},
		{input: { c1: 1, c2: 0, c3: 0 ,c4: 0, c5: 1}, output: { pareja: 1 }},
		{input: { c1: 0, c2: 1, c3: 1 ,c4: 0, c5: 0}, output: { pareja: 1 }},
		{input: { c1: 2, c2: 2, c3: 0 ,c4: 0, c5: 0}, output: { pareja: 1 }}, 
		{input: { c1: 0, c2: 0, c3: 0 ,c4: 1, c5: 1}, output: { pareja: 1 }}, // Resuelto 
/*Doblepareja*/
		{input: { c1: 1, c2: 1, c3: 2 ,c4: 2, c5: 0}, output: { DoblePareja: 1 }},
		{input: { c1: 2, c2: 2, c3: 1 ,c4: 1, c5: 0}, output: { DoblePareja: 1 }}, 
		{input: { c1: 0, c2: 1, c3: 1 ,c4: 2, c5: 2}, output: { DoblePareja: 1 }}, 
		{input: { c1: 1, c2: 1, c3: 0 ,c4: 2, c5: 2}, output: { DoblePareja: 1 }},
		{input: { c1: 0, c2: 0, c3: 1 ,c4: 2, c5: 2}, output: { DoblePareja: 1 }}, 		
		{input: { c1: 0, c2: 2, c3: 2 ,c4: 1, c5: 1}, output: { DoblePareja: 1 }}, // Resuelto 
/*Trio*/
		{input: { c1: 1, c2: 1, c3: 1 ,c4: 0, c5: 0}, output: { Trio: 1 }},
		{input: { c1: 0, c2: 0, c3: 1 ,c4: 1, c5: 1}, output: { Trio: 1 }},
		{input: { c1: 0, c2: 1, c3: 1 ,c4: 1, c5: 0}, output: { Trio: 1 }},
		{input: { c1: 1, c2: 0, c3: 1 ,c4: 1, c5: 0}, output: { Trio: 1 }},
		{input: { c1: 0, c2: 1, c3: 0 ,c4: 1, c5: 1}, output: { Trio: 1 }},
		{input: { c1: 2, c2: 2, c3: 2 ,c4: 0, c5: 0}, output: { Trio: 1 }},
		{input: { c1: 0, c2: 2, c3: 2 ,c4: 2, c5: 0}, output: { Trio: 1 }}, 
		{input: { c1: 1, c2: 0, c3: 0 ,c4: 1, c5: 1}, output: { Trio: 1 }}, // Resuelto
		
		   
		   ];

net.train(data, {
  errorThresh: 0.005,  // error threshold to reach
  iterations: 20000,   // maximum training iterations
  log: false,           // console.log() progress periodically
  logPeriod: 10,       // number of iterations between logging
  learningRate: 0.9  // learning rate
});

var output = net.run({ c1: 2, c2: 0, c3: 1,c4: 1, c5: 2});  /*Muestra*/

console.log((memoria.freemem()/1024)/1024); /*memoria en uso despues del proceso*/

console.log("Jugada>>",output);


