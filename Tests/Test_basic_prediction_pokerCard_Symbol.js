console.log("**Test NeuralNetwork**");

var moduloN = require('brain');
var memoria = require('os');

console.log((memoria.freemem()/1024)/1024); /*memoria en uso despues del proceso*/

var net = new moduloN.NeuralNetwork(  {hiddenLayers: [5]});

/*Muestras necesarias para el aprendizaje*/
var data=[

/*Sin jugada*/
		{input: { c1: █, c2: 0, c3: 0 ,c4: 0, c5: 0}, output: { nada: 1 }},
		{input: { c1: 0, c2: █, c3: 0 ,c4: 0, c5: 0}, output: { nada: 0 }},
		{input: { c1: 0, c2: 0, c3: █ ,c4: 0, c5: 0}, output: { nada: 1 }},
		{input: { c1: 0, c2: 0, c3: 0 ,c4: █, c5: 0}, output: { nada: 0 }},
		{input: { c1: 0, c2: 0, c3: 0 ,c4: 0, c5: █}, output: { nada: 1 }},
/*pareja*/
		{input: { c1: █, c2: █, c3: 0 ,c4: 0, c5: 0}, output: { pareja: 1 }},
		{input: { c1: █, c2: 0, c3: █ ,c4: 0, c5: 0}, output: { pareja: 1 }},
		{input: { c1: █, c2: 0, c3: 0 ,c4: █, c5: 0}, output: { pareja: 1 }},
		{input: { c1: █, c2: 0, c3: 0 ,c4: 0, c5: █}, output: { pareja: 1 }},
		{input: { c1: 0, c2: █, c3: █ ,c4: 0, c5: 0}, output: { pareja: 1 }},
		{input: { c1: ●, c2: ●, c3: 0 ,c4: 0, c5: 0}, output: { pareja: 1 }}, 
		{input: { c1: 0, c2: 0, c3: 0 ,c4: █, c5: █}, output: { pareja: 1 }}, // Resuelto 
/*Doblepareja*/
		{input: { c1: █, c2: █, c3: ● ,c4: ●, c5: 0}, output: { DoblePareja: 1 }},
		{input: { c1: ●, c2: ●, c3: █ ,c4: █, c5: 0}, output: { DoblePareja: 1 }}, 
		{input: { c1: 0, c2: █, c3: █ ,c4: ●, c5: ●}, output: { DoblePareja: 1 }}, 
		{input: { c1: 0, c2: ●, c3: ● ,c4: █, c5: █}, output: { DoblePareja: 1 }}, // Resuelto 
/*Trio*/
		{input: { c1: █, c2: █, c3: █ ,c4: 0, c5: 0}, output: { Trio: 1 }},
		{input: { c1: 0, c2: 0, c3: █ ,c4: █, c5: █}, output: { Trio: 1 }},
		{input: { c1: 0, c2: █, c3: █ ,c4: █, c5: 0}, output: { Trio: 1 }},
		{input: { c1: █, c2: 0, c3: █ ,c4: █, c5: 0}, output: { Trio: 1 }},
		{input: { c1: 0, c2: █, c3: 0 ,c4: █, c5: █}, output: { Trio: 1 }},
		{input: { c1: ●, c2: ●, c3: ● ,c4: 0, c5: 0}, output: { Trio: 1 }},
		{input: { c1: 0, c2: ●, c3: ● ,c4: ●, c5: 0}, output: { Trio: 1 }}, 
		{input: { c1: █, c2: 0, c3: 0 ,c4: █, c5: █}, output: { Trio: 1 }}, // Resuelto
		
		   
		   ];

net.train(data, {
  errorThresh: 0.005,  // error threshold to reach
  iterations: 20000,   // maximum training iterations
  log: false,           // console.log() progress periodically
  logPeriod: 10,       // number of iterations between logging
  learningRate: 0.3  // learning rate
});

var output = net.run({ c1: ●, c2: 0, c3: 0,c4: 0, c5: ●});  /*Muestra*/

console.log((memoria.freemem()/1024)/1024); /*memoria en uso despues del proceso*/

console.log("Jugada>>",output);


