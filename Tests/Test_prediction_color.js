console.log("**Test NeuralNetwork**");

var moduloN = require('brain');
var memoria = require('os');

console.log((memoria.freemem()/1024)/1024);

var net = new moduloN.NeuralNetwork(  {hiddenLayers: [4]});

var data=[{input: { r: 0, g: 0, b: 0 }, output: { black: 1 }},
           {input: { r: 44, g: 62, b: 80 }, output: { black: 1 }},
           {input: { r: 255, g: 255, b: 255 }, output: { white: 1 }}];

net.train(data, {
  errorThresh: 0.005,  // error threshold to reach
  iterations: 20000,   // maximum training iterations
  log: false,           // console.log() progress periodically
  logPeriod: 10,       // number of iterations between logging
  learningRate: 0.3  // learning rate
});

var output = net.run({ r: 95, g: 106, b: 106 });  // { white: 0.99, black: 0.002 }

console.log((memoria.freemem()/1024)/1024);

console.log(">>",output);


